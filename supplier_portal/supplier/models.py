# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Company(models.Model):
    company_name = models.CharField(max_length=255)
    company_mobile = models.BigIntegerField()
    company_address = models.CharField(max_length=255)

class Supplier(models.Model):
    name = models.CharField(max_length=255)
    mobile = models.BigIntegerField()
    password = models.CharField(max_length=255)
    company_Id = models.ForeignKey(Company)

class Purchase_order(models.Model):
    po_entry = models.TextField(max_length=255)
    po_number = models.BigIntegerField()
    po_date = models.DateField(null=True, blank=True)
    po_status = models.BooleanField(default=False)
    Amount = models.BigIntegerField()
    price = models.BigIntegerField()
    item_code = models.BigIntegerField()
    item_description = models.TextField()
    item_quantity = models.BigIntegerField()
    supllier_id = models.ForeignKey(Supplier)


