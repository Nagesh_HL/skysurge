from django.conf.urls import url

from .views import dating_data

urlpatterns = [
    url(r'^fetchdatedata', dating_data)
]