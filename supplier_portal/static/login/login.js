angular.module('login', ['ui.bootstrap','ui.router','ngAnimate']);

angular.module('login').config(function($stateProvider) {

    /* Add New States Above */
    $stateProvider.state('home', {
        url: '/home',
        templateUrl: 'login/partial/register/login.html',
        controller: 'RegisterCtrl'
    });
    $stateProvider.state('register', {
        url: '/register',
        templateUrl: 'login/partial/register/register.html',
        controller: 'RegisterCtrl'
    });
    $stateProvider.state('sidenav', {
        url: '/sidenav',
        templateUrl: 'supplier/partial/dashboard/sidenav.html',
        controller: 'DashboardCtrl'
    }).state('sidenav.dashboard', {
        url: '/dashboard',
        templateUrl: 'supplier/partial/dashboard/dashboard.html',
        controller: 'DashboardCtrl'
    }).state('sidenav.purchaseorder', {
        url: '/purchaseorder',
        templateUrl: 'supplier/partial/dashboard/purchaseorder.html',
        controller: 'DashboardCtrl'
    }).state('sidenav.GoodsReceipt', {
        url: '/GoodsReceipt',
        templateUrl: 'supplier/partial/dashboard/GoodsReceipt.html',
        controller: 'DashboardCtrl'
    }).state('sidenav.GateEntry', {
        url: '/GateEntry',
        templateUrl: 'supplier/partial/dashboard/GateEntry.html',
        controller: 'DashboardCtrl'
    });

});

