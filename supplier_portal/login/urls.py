from django.conf.urls import url

from .views import register, signin, getcompany

urlpatterns =[
    url(r'^register',register),
    url(r'^signin', signin),
    url(r'^getcompany', getcompany)
]